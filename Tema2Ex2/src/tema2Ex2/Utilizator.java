package tema2Ex2;

public class Utilizator {
	private String cnp;
	private String nume;
	private String prenume;
	private String localitateaDeDomiciliu;
	private long numar;
	public Utilizator() {}

	public Utilizator(String cnp, String nume, String prenume, String localitateaDeDomiciliu, long numar) {
		this.cnp = cnp;
		this.nume = nume;
		this.prenume = prenume;
		this.localitateaDeDomiciliu = localitateaDeDomiciliu;
		this.numar = numar;
	}

	public String getCnp() {
		return cnp;
	}

	public void setCnp(String cnp) {
		this.cnp = cnp;
	}

	public String getNume() {
		return nume;
	}

	public void setNume(String nume) {
		this.nume = nume;
	}

	public String getPrenume() {
		return prenume;
	}

	public void setPrenume(String prenume) {
		this.prenume = prenume;
	}

	public String getLocalitateaDeDomiciliu() {
		return localitateaDeDomiciliu;
	}

	public void setLocalitateaDeDomiciliu(String localitateaDeDomiciliu) {
		this.localitateaDeDomiciliu = localitateaDeDomiciliu;
	}

	public Long getNumar() {
		return numar;
	}

	public void setNumar(Long numar) {
		this.numar = numar;
	}

	@Override
	public String toString() {
		return "Utilizator cnp=" + cnp + ", nume=" + nume + ", prenume=" + prenume + ", localitateaDeDomiciliu="
				+ localitateaDeDomiciliu + ", numar=" + numar;
	}
	
	
}
