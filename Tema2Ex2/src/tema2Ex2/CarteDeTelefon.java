package tema2Ex2;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class CarteDeTelefon {
	 
	public static void start() {
		main(null);
	}
	
	public static void main(String[] args) {
		List<Utilizator> CarteDeTelefon = new ArrayList<>();
		Utilizator u1 = new Utilizator("2990703359216", "Andreea", "Popescu", "Timisoara", 736294729);
		Utilizator u2 = new Utilizator("2990703359240", "Camelia", "Catalin", "Timisoara", 759204782);
		Utilizator u3 = new Utilizator("2990703356827", "Ilinca", "Mihail", "Timisoara", 750239485);
		Utilizator u4 = new Utilizator("2990703351414", "Lucian", "Petrut", "Timisoara", 739846503);
		Utilizator u5 = new Utilizator("2990703354858", "Remus", "Augustin", "Timisoara", 720938485);
		Utilizator u6 = new Utilizator("2990703350340", "Catrinel", "Theodor", "Timisoara", 729387458);
		Utilizator u7 = new Utilizator("2990703351473", "Rodica", "Lucian", "Timisoara", 738475875);
		Utilizator u8 = new Utilizator("2990703358451", "Flaviu", "Minodora", "Timisoara", 755865688);
		Utilizator u9 = new Utilizator("2990703359718", "Decebal", "Atanasie", "Timisoara", 796509689);
		Utilizator u10 = new Utilizator("2990703353872", "Paula", "Haralamb", "Timisoara", 739485394);
		
		CarteDeTelefon.add(u1);
		CarteDeTelefon.add(u2);
		CarteDeTelefon.add(u3);
		CarteDeTelefon.add(u4);
		CarteDeTelefon.add(u5);
		CarteDeTelefon.add(u6);
		CarteDeTelefon.add(u7);
		CarteDeTelefon.add(u8);
		CarteDeTelefon.add(u9);
		CarteDeTelefon.add(u10);
		
		System.out.println("Pentru a inchide, stasteaza '0' ");
		System.out.println("Introduceti un numar de telefon: ");
		Scanner scan = new Scanner(System.in);
		long numar = scan.nextLong();
		int found = 0;
		if(numar == 0){
			System.out.println("Programul s-a inchis");
			return;
		}
		for(Utilizator u : CarteDeTelefon) {
			if(numar == u.getNumar()) {
				System.out.println(u.toString());
				start();
				found = 1;
			}
		}
		if(found == 0) {
			System.out.println("Numarul nu exista");
			start();
		}
	}
}
