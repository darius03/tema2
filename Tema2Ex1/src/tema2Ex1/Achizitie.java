package tema2Ex1;

public class Achizitie {
	private double pretPerUnitate;
	private int cantitate;
	public Achizitie() {}
	public Achizitie (double pretPerUnitate, int cantitate) {
		this.pretPerUnitate = pretPerUnitate;
		this.cantitate = cantitate;
		
	}
	public double getPretPerUnitate() {
		return pretPerUnitate;
	}
	public void setPretPerUnitate(double pretPerUnitate) {
		this.pretPerUnitate = pretPerUnitate;
	}
	public int getCantitate() {
		return cantitate;
	}
	public void setCantitate(int cantitate) {
		this.cantitate = cantitate;
	}
	
	public double getPret() {
		return pretPerUnitate * cantitate;
	}
	@Override
	public String toString() {
		return "Achizitie [pretPerUnitate=" + pretPerUnitate + ", cantitate=" + cantitate + "]";
	}
	
}
