package tema2Ex1;

public class Aliment extends Achizitie{
	private String nume;
	private int dataExpirare;
	
	public Aliment() {}
	
	public Aliment(String nume, int dataExpirare) {
		this.nume =nume;
		this.dataExpirare = dataExpirare;
	}

	public String getNume() {
		return nume;
	}

	public void setNume(String nume) {
		this.nume = nume;
	}

	public int getDataExpirare() {
		return dataExpirare;
	}

	public void setDataExpirare(int dataExpirare) {
		this.dataExpirare = dataExpirare;
	}

	@Override
	public String toString() {
		return "Aliment [nume=" + nume + ", dataExpirare=" + dataExpirare + " Pret " + getPret();
	}
	
}
