package tema2Ex1;

public class AparatElectrocasnic extends Achizitie{
	private String nume;
	private int garantie;
	
	public AparatElectrocasnic() {}
	
	public AparatElectrocasnic(String nume, int garantie) {
		this.nume = nume;
		this.garantie = garantie;
	}

	public String getNume() {
		return nume;
	}

	public void setNume(String nume) {
		this.nume = nume;
	}

	public int getGarantie() {
		return garantie;
	}

	public void setGarantie(int garantie) {
		this.garantie = garantie;
	}

	@Override
	public String toString() {
		return "AparatElectrocasnic [nume=" + nume + ", garantie=" + garantie + " Pret " + getPret();
	}
	
}
