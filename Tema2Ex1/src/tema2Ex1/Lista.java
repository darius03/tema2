package tema2Ex1;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class Lista extends Achizitie {
	
	public static void main(String[] args) {
		Achizitie a1 = new Aliment("Faina", 25);
		Achizitie a2 = new AparatElectrocasnic("Robot de bucatarie", 2);
		Achizitie a3 = new Aliment("Paste", 29);
		Achizitie a4 = new AparatElectrocasnic("Mixer de mana", 1);
		Achizitie a5 = new Aliment("Legume uscate", 19);
		Achizitie a6 = new Aliment("Zahar", 30);
		Achizitie a7 = new AparatElectrocasnic("Cuptor cu microunde", 5);
		Achizitie a8 = new Aliment("Malai", 15);
		Achizitie a9 = new Aliment("Sos de tomate ", 23);
		Achizitie a10 = new Aliment("Gris", 18);
			
		a1.setCantitate(1);
		a1.setPretPerUnitate(9.99);
		a2.setCantitate(1);
		a2.setPretPerUnitate(100);
		a3.setCantitate(3);
		a3.setPretPerUnitate(3);
		a4.setCantitate(1);
		a4.setPretPerUnitate(80);
		a5.setCantitate(1);
		a5.setPretPerUnitate(3);
		a6.setCantitate(3);
		a6.setPretPerUnitate(5);
		a7.setCantitate(1);
		a7.setPretPerUnitate(300);
		a8.setCantitate(1);
		a8.setPretPerUnitate(2);
		a9.setCantitate(1);
		a9.setPretPerUnitate(7);
		a10.setCantitate(1);
		a10.setPretPerUnitate(1);
		
		List<Achizitie> ListaCumparaturi = new ArrayList<>();
		ListaCumparaturi.add(a1);
		ListaCumparaturi.add(a2);
		ListaCumparaturi.add(a3);
		ListaCumparaturi.add(a4);
		ListaCumparaturi.add(a5);
		ListaCumparaturi.add(a6);
		ListaCumparaturi.add(a7);
		ListaCumparaturi.add(a8);
		ListaCumparaturi.add(a9);
		ListaCumparaturi.add(a10);
		
		Comparator<Achizitie> byPret = new Comparator<Achizitie>() {
			public int compare (Achizitie a, Achizitie b) {
				return (int) (a.getPret() - b.getPret());
			}
		};
		
		Collections.sort(ListaCumparaturi, byPret);
		
		for(Achizitie achizitie : ListaCumparaturi) {
			System.out.println(achizitie.toString() + "\n");
		}
		
		
	}
}
